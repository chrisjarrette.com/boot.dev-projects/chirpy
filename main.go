package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"golang.org/x/crypto/bcrypt"

	"github.com/go-chi/chi/v5"
	"github.com/golang-jwt/jwt/v5"
	"github.com/joho/godotenv"
	"gitlab.com/chrisjarrette.com/boot.dev-projects/chirpy/database"
)

type apiConfig struct {
	fileserverHits int
	db             *database.DB
	jwtSecret      string
	apiKey         string
}

func main() {
	dbg := flag.Bool("debug", false, "Enable debug mode")
	flag.Parse()

	godotenv.Load()
	jwtSecret := os.Getenv("JWT_SECRET")
	polkaApiKey := os.Getenv("POLKA_API_KEY")

	cwd, err := os.Getwd()
	if err != nil {
		log.Fatal("Could not get current directory")
		return
	}

	db, err := database.NewDB(cwd, *dbg)
	if err != nil {
		log.Fatal("Could not open database connection")
		return
	}

	config := apiConfig{
		fileserverHits: 0,
		db:             db,
		jwtSecret:      jwtSecret,
		apiKey:         polkaApiKey,
	}

	port := 8080

	fs := http.FileServer(http.Dir("."))

	r := chi.NewRouter()
	r_api := chi.NewRouter()
	r_admin := chi.NewRouter()

	fsHandler := config.middlewareMetricsInc(http.StripPrefix("/app", fs))
	r.Handle("/app", fsHandler)
	r.Handle("/app/*", fsHandler)

	r_api.Get("/healthz", readinessCheck)
	r_api.Get("/chirps", config.getChirps)
	r_api.Get("/chirps/{id}", config.getChirp)
	r_api.Post("/chirps", config.postChirp)
	r_api.Delete("/chirps/{id}", config.deleteChirp)
	r_api.Post("/users", config.createUser)
	r_api.Put("/users", config.updateUser)
	r_api.Post("/login", config.authUser)
	r_api.Post("/refresh", config.refreshAccess)
	r_api.Post("/revoke", config.revokeRefresh)

	r_api.Post("/polka/webhooks", config.handleWebhook)

	r.Mount("/api", r_api)

	r_admin.Get("/metrics", config.fsHitsCount)
	r.Mount("/admin", r_admin)

	corsMux := middlewareCors(r)

	server := http.Server{
		Addr:    ":" + fmt.Sprint(port),
		Handler: corsMux,
	}

	fmt.Printf("Server listening on port %d\n", port)
	log.Fatal(server.ListenAndServe())
}

func middlewareCors(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "*")
		if r.Method == "OPTIONS" {
			w.WriteHeader(http.StatusOK)
			return
		}
		next.ServeHTTP(w, r)
	})
}

func RespondWithError(w http.ResponseWriter, code int, msg string) {
	type ErrorBody struct {
		Error string `json:"error"`
	}

	error := ErrorBody{
		Error: msg,
	}

	resp, err := json.Marshal(error)
	if err != nil {
		w.WriteHeader(500)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(resp)
}

func RespondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	resp, err := json.Marshal(payload)
	if err != nil {
		w.WriteHeader(500)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(resp)
}

func (cfg *apiConfig) fsHitsCount(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(fmt.Sprintf("<html>\n<body>\n<h1>Welcome, Chirpy Admin</h1>\n<p>Chirpy has been visited %d times!</p>\n</body>\n</html>", cfg.fileserverHits)))
}

func (cfg *apiConfig) middlewareMetricsInc(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		cfg.fileserverHits++
		next.ServeHTTP(w, r)
	})
}

func readinessCheck(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("OK"))
}

func (c *apiConfig) getChirp(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		RespondWithError(w, 400, "Invalid ID")
	}

	chirp, err := c.db.GetChirp(id)
	if err != nil {
		RespondWithError(w, 400, "Something went wrong")
	}

	if chirp == (database.Chirp{}) {
		RespondWithError(w, 404, "Chirp not found")
	} else {
		RespondWithJSON(w, 200, chirp)
	}
}

func (c *apiConfig) getChirps(w http.ResponseWriter, r *http.Request) {
	author_id := 0
	sortAsc := true

	if len(r.URL.Query().Get("author_id")) > 0 {
		var err error
		author_id, err = strconv.Atoi(r.URL.Query().Get("author_id"))
		if err != nil {
			RespondWithError(w, 400, "Something went wrong")
			return
		}
	}

	if len(r.URL.Query().Get("sort")) > 0 {
		if r.URL.Query().Get("sort") == "desc" {
			sortAsc = false
		}
	}

	chirps, err := c.db.GetChirps(author_id, sortAsc)
	if err != nil {
		RespondWithError(w, 400, "Something went wrong")
		return
	}

	RespondWithJSON(w, 200, chirps)
}

func (c *apiConfig) postChirp(w http.ResponseWriter, r *http.Request) {
	type Parameters struct {
		Body string `json:"body"`
	}

	decoder := json.NewDecoder(r.Body)
	params := Parameters{}
	err := decoder.Decode(&params)
	if err != nil {
		RespondWithError(w, 400, "Something went wrong")
		return
	}

	headers := strings.Split(r.Header.Get("Authorization"), " ")
	if len(headers) < 2 {
		RespondWithError(w, 401, "Unauthorized")
		return
	}

	token_string := headers[1]

	claims := jwt.RegisteredClaims{}
	token, err := jwt.ParseWithClaims(token_string, &claims, func(token *jwt.Token) (interface{}, error) { return []byte(c.jwtSecret), nil })
	if err != nil {
		RespondWithError(w, 401, "Unauthorized")
		return
	}

	if !token.Valid || claims.ExpiresAt.Time.Before(time.Now()) || claims.Issuer != "chirpy-access" {
		RespondWithError(w, 401, "Unauthorized")
		return
	}

	if len(params.Body) <= 140 {
		author_id, err := strconv.Atoi(claims.Subject)
		if err != nil {
			RespondWithError(w, 400, "Something went wrong")
			return
		}

		resp, err := c.db.CreateChirp(replaceProfanity(params.Body), author_id)
		if err != nil {
			RespondWithError(w, 400, "Something went wrong")
			return
		}

		RespondWithJSON(w, 201, resp)
	} else {
		RespondWithError(w, 400, "Chirp is too long")
	}
}

func (c *apiConfig) deleteChirp(w http.ResponseWriter, r *http.Request) {
	chirp_id, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		RespondWithError(w, 404, "Invalid Chirp ID")
		return
	}

	type ResponseBody struct {
		Status string `json:"status"`
	}

	headers := strings.Split(r.Header.Get("Authorization"), " ")
	if len(headers) < 2 {
		RespondWithError(w, 401, "Unauthorized")
		return
	}

	token_string := headers[1]

	claims := jwt.RegisteredClaims{}
	token, err := jwt.ParseWithClaims(token_string, &claims, func(token *jwt.Token) (interface{}, error) { return []byte(c.jwtSecret), nil })
	if err != nil {
		RespondWithError(w, 401, "Unauthorized")
		return
	}

	if !token.Valid || claims.ExpiresAt.Time.Before(time.Now()) || claims.Issuer != "chirpy-access" {
		RespondWithError(w, 401, "Unauthorized")
		return
	}

	author_id, err := strconv.Atoi(claims.Subject)
	if err != nil {
		RespondWithError(w, 400, "Something went wrong")
		return
	}

	err = c.db.DeleteChirp(author_id, chirp_id)
	if err != nil {
		if err.Error() == "forbidden" {
			RespondWithError(w, 403, "Forbidden")
			return
		}

		if err.Error() == "chirp not found" {
			RespondWithError(w, 404, "Chirp not found")
			return
		}

		RespondWithError(w, 400, "Something went wrong")
		return
	}

	RespondWithJSON(w, 200, ResponseBody{Status: "ok"})
}

func replaceProfanity(msg string) string {
	words := strings.Split(msg, " ")

	bad_words := []string{"kerfuffle", "sharbert", "fornax"}

	for i, word := range words {
		for _, bad_word := range bad_words {
			if strings.ToLower(word) == bad_word {
				words[i] = "****"
			}
		}
	}

	return strings.Join(words, " ")
}

func (c *apiConfig) createUser(w http.ResponseWriter, r *http.Request) {
	type Parameters struct {
		Pwd   string `json:"password"`
		Email string `json:"email"`
	}

	type UserResponse struct {
		ID        int    `json:"id"`
		Email     string `json:"email"`
		ChirpyRed bool   `json:"is_chirpy_red"`
	}

	decoder := json.NewDecoder(r.Body)
	params := Parameters{}
	err := decoder.Decode(&params)
	if err != nil {
		RespondWithError(w, 400, "Something went wrong")
		return
	}

	if (len(params.Email) > 0) && (len(params.Pwd) > 0) {
		hash, err := bcrypt.GenerateFromPassword([]byte(params.Pwd), 4)
		if err != nil {
			RespondWithError(w, 400, "Something went wrong")
		}

		resp, err := c.db.CreateUser(params.Email, hash)
		if err != nil {
			RespondWithError(w, 400, "Something went wrong")
			return
		}

		RespondWithJSON(w, 201, UserResponse{ID: resp.ID, Email: resp.Email, ChirpyRed: resp.IsChirpyRed})
	} else {
		RespondWithError(w, 400, "Something went wrong")
	}
}

func (c *apiConfig) authUser(w http.ResponseWriter, r *http.Request) {
	type Parameters struct {
		Pwd     string `json:"password"`
		Email   string `json:"email"`
		Expires int    `json:"expires_in_seconds,omitempty"`
	}

	type UserResponse struct {
		ID        int    `json:"id"`
		Email     string `json:"email"`
		ChirpyRed bool   `json:"is_chirpy_red"`
		Token     string `json:"token"`
		Refresh   string `json:"refresh_token"`
	}

	decoder := json.NewDecoder(r.Body)
	params := Parameters{}
	err := decoder.Decode(&params)
	if err != nil {
		RespondWithError(w, 400, "Something went wrong")
		return
	}

	if (len(params.Email) > 0) && (len(params.Pwd) > 0) {
		resp, err := c.db.GetUser(params.Email)
		if err != nil {
			RespondWithError(w, 400, "Something went wrong")
			return
		}

		err = bcrypt.CompareHashAndPassword(resp.Password, []byte(params.Pwd))
		if err != nil {
			RespondWithError(w, 401, "Unauthorized")
			return
		}

		access_claims := jwt.RegisteredClaims{
			Issuer:    "chirpy-access",
			IssuedAt:  jwt.NewNumericDate(time.Now()),
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(time.Duration(1) * time.Hour)),
			Subject:   fmt.Sprintf("%d", resp.ID),
		}

		token, err := jwt.NewWithClaims(jwt.SigningMethodHS256, access_claims).SignedString([]byte(c.jwtSecret))
		if err != nil {
			RespondWithError(w, 400, "Something went wrong")
			return
		}

		refresh_claims := jwt.RegisteredClaims{
			Issuer:    "chirpy-refresh",
			IssuedAt:  jwt.NewNumericDate(time.Now()),
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(time.Duration(24*60) * time.Hour)),
			Subject:   fmt.Sprintf("%d", resp.ID),
		}

		refresh, err := jwt.NewWithClaims(jwt.SigningMethodHS256, refresh_claims).SignedString([]byte(c.jwtSecret))
		if err != nil {
			RespondWithError(w, 400, "Something went wrong")
			return
		}

		RespondWithJSON(w, 200, UserResponse{ID: resp.ID, Email: resp.Email, ChirpyRed: resp.IsChirpyRed, Token: token, Refresh: refresh})
	} else {
		RespondWithError(w, 400, "Something went wrong")
	}
}

func (c *apiConfig) updateUser(w http.ResponseWriter, r *http.Request) {
	type Parameters struct {
		Pwd   string `json:"password"`
		Email string `json:"email"`
	}

	type UserResponse struct {
		ID        int    `json:"id"`
		Email     string `json:"email"`
		ChirpyRed bool   `json:"is_chirpy_red"`
	}

	decoder := json.NewDecoder(r.Body)
	params := Parameters{}
	err := decoder.Decode(&params)
	if err != nil {
		RespondWithError(w, 400, "Something went wrong")
		return
	}

	headers := strings.Split(r.Header.Get("Authorization"), " ")
	if len(headers) < 2 {
		RespondWithError(w, 401, "Unauthorized")
		return
	}

	token_string := headers[1]

	claims := jwt.RegisteredClaims{}
	token, err := jwt.ParseWithClaims(token_string, &claims, func(token *jwt.Token) (interface{}, error) { return []byte(c.jwtSecret), nil })
	if err != nil {
		RespondWithError(w, 401, "Unauthorized")
		return
	}

	if !token.Valid || claims.ExpiresAt.Time.Before(time.Now()) || claims.Issuer != "chirpy-access" {
		RespondWithError(w, 401, "Unauthorized")
		return
	}

	id, err := strconv.Atoi(claims.Subject)
	if err != nil {
		RespondWithError(w, 500, "Something went wrong")
	}

	if (len(params.Email) > 0) && (len(params.Pwd) > 0) {
		hash, err := bcrypt.GenerateFromPassword([]byte(params.Pwd), 4)
		if err != nil {
			RespondWithError(w, 400, "Something went wrong")
		}

		resp, err := c.db.UpdateUser(id, params.Email, hash)
		if err != nil {
			RespondWithError(w, 400, "Something went wrong")
			return
		}

		RespondWithJSON(w, 200, UserResponse{ID: resp.ID, Email: resp.Email, ChirpyRed: resp.IsChirpyRed})
	} else {
		RespondWithError(w, 400, "Something went wrong")
	}
}

func (c *apiConfig) refreshAccess(w http.ResponseWriter, r *http.Request) {
	type TokenResponse struct {
		Token string `json:"token"`
	}

	headers := strings.Split(r.Header.Get("Authorization"), " ")
	if len(headers) < 2 {
		RespondWithError(w, 401, "Unauthorized")
		return
	}

	token_string := headers[1]

	claims := jwt.RegisteredClaims{}
	token, err := jwt.ParseWithClaims(token_string, &claims, func(token *jwt.Token) (interface{}, error) { return []byte(c.jwtSecret), nil })
	if err != nil {
		RespondWithError(w, 401, "Unauthorized")
		return
	}

	if !token.Valid || claims.ExpiresAt.Time.Before(time.Now()) || claims.Issuer != "chirpy-refresh" {
		RespondWithError(w, 401, "Unauthorized")
		return
	}

	revoked, err := c.db.CheckRevoked(token_string)
	if err != nil {
		RespondWithError(w, 400, "Something went wrong")
		return
	}
	if revoked != (database.Token{}) {
		RespondWithError(w, 401, "Unauthorized")
		return
	}

	access_claims := jwt.RegisteredClaims{
		Issuer:    "chirpy-access",
		IssuedAt:  jwt.NewNumericDate(time.Now()),
		ExpiresAt: jwt.NewNumericDate(time.Now().Add(time.Duration(1) * time.Hour)),
		Subject:   claims.Subject,
	}

	access, err := jwt.NewWithClaims(jwt.SigningMethodHS256, access_claims).SignedString([]byte(c.jwtSecret))
	if err != nil {
		RespondWithError(w, 400, "Something went wrong")
		return
	}

	RespondWithJSON(w, 200, TokenResponse{Token: access})
}

func (c *apiConfig) revokeRefresh(w http.ResponseWriter, r *http.Request) {
	type RevokedResponse struct {
		Revoked    string    `json:"revoked_token"`
		RevokeTime time.Time `json:"revoked_at"`
	}

	headers := strings.Split(r.Header.Get("Authorization"), " ")
	if len(headers) < 2 {
		RespondWithError(w, 400, "Something went wrong")
		return
	}

	token_string := headers[1]

	claims := jwt.RegisteredClaims{}
	token, err := jwt.ParseWithClaims(token_string, &claims, func(token *jwt.Token) (interface{}, error) { return []byte(c.jwtSecret), nil })
	if err != nil {
		RespondWithError(w, 400, "Something went wrong")
		return
	}

	if !token.Valid || claims.ExpiresAt.Time.Before(time.Now()) || claims.Issuer != "chirpy-refresh" {
		RespondWithError(w, 400, "Something went wrong")
		return
	}

	revoked, err := c.db.RevokeToken(token_string)
	if err != nil {
		RespondWithError(w, 400, "Something went wrong")
		return
	}

	if revoked == (database.Token{}) {
		RespondWithError(w, 400, "Something went wrong")
		return
	}

	RespondWithJSON(w, 200, RevokedResponse{Revoked: revoked.ID, RevokeTime: revoked.RevokeTime})
}

func (c *apiConfig) handleWebhook(w http.ResponseWriter, r *http.Request) {
	type Parameters struct {
		Event string `json:"event"`
		Data  struct {
			UserID int `json:"user_id"`
		} `json:"data"`
	}

	type ResponseBody struct {
		Status string `json:"status"`
	}

	headers := strings.Split(r.Header.Get("Authorization"), " ")
	if len(headers) < 2 || headers[1] != c.apiKey {
		RespondWithError(w, 401, "Unauthorized")
		return
	}

	decoder := json.NewDecoder(r.Body)
	params := Parameters{}
	err := decoder.Decode(&params)
	if err != nil {
		RespondWithError(w, 400, "Something went wrong")
		return
	}

	if params.Event == "user.upgraded" {
		err = c.db.UpgradeUser(params.Data.UserID)
		if err != nil {
			RespondWithError(w, 404, "user not found")
			return
		}
	}

	RespondWithJSON(w, 200, ResponseBody{})
}
