package database

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"sort"
	"sync"
	"time"
)

type Chirp struct {
	ID       int    `json:"id"`
	Body     string `json:"body"`
	AuthorID int    `json:"author_id"`
}

type User struct {
	ID          int    `json:"id"`
	Email       string `json:"email"`
	IsChirpyRed bool   `json:"is_chirpy_red"`
	Password    []byte `json:"password"`
}

type Token struct {
	ID         string    `json:"id"`
	RevokeTime time.Time `json:"revoke_time"`
}

type DB struct {
	path string
	mux  sync.Mutex
}

type DBStructure struct {
	Chirps  map[int]Chirp    `json:"chirps"`
	Users   map[int]User     `json:"users"`
	Revoked map[string]Token `json:"revoked_tokens"`
}

func NewDB(path string, debug bool) (*DB, error) {
	db := DB{
		path: filepath.Join(path, "database.json"),
	}

	if debug {
		os.Remove(db.path)
	}

	err := db.ensureDB()
	if err != nil {
		return nil, err
	}

	return &db, nil
}

func (db *DB) CreateChirp(body string, author int) (Chirp, error) {
	dbs, err := db.loadDB()
	if err != nil {
		return Chirp{}, err
	}

	var id int
	var ids []int
	for k := range dbs.Chirps {
		ids = append(ids, dbs.Chirps[k].ID)
	}

	if len(ids) == 0 {
		id = 1
	} else {
		sort.Slice(ids, func(i, j int) bool { return ids[i] < ids[j] })
		id = ids[len(ids)-1] + 1
	}

	chirp := Chirp{
		ID:       id,
		Body:     body,
		AuthorID: author,
	}

	dbs.Chirps[id] = chirp

	err = db.writeDB(dbs)
	if err != nil {
		return Chirp{}, err
	}

	return chirp, nil
}

func (db *DB) GetChirp(id int) (Chirp, error) {
	dbs, err := db.loadDB()
	if err != nil {
		return Chirp{}, err
	}

	for _, c := range dbs.Chirps {
		if c.ID == id {
			return c, nil
		}
	}

	return Chirp{}, err
}

func (db *DB) DeleteChirp(author_id, chirp_id int) error {
	dbs, err := db.loadDB()
	if err != nil {
		return errors.New("something went wrong")
	}

	for i, c := range dbs.Chirps {
		if c.ID == chirp_id {
			if c.AuthorID == author_id {
				delete(dbs.Chirps, i)
				return nil
			} else {
				return errors.New("forbidden")
			}
		}
	}

	return errors.New("chirp not found")
}

func (db *DB) GetChirps(author_id int, sortAsc bool) ([]Chirp, error) {
	dbs, err := db.loadDB()
	if err != nil {
		return nil, err
	}

	var keys []int
	for k := range dbs.Chirps {
		if author_id > 0 {
			if dbs.Chirps[k].AuthorID == author_id {
				keys = append(keys, k)
			}
		} else {
			keys = append(keys, k)
		}
	}

	if len(keys) == 0 {
		return []Chirp{}, nil
	} else {
		var sortFunc func(i, j int) bool
		if sortAsc {
			sortFunc = func(i, j int) bool { return dbs.Chirps[keys[i]].ID < dbs.Chirps[keys[j]].ID }
		} else {
			sortFunc = func(i, j int) bool { return dbs.Chirps[keys[i]].ID > dbs.Chirps[keys[j]].ID }
		}

		sort.Slice(keys, sortFunc)
		chirps := make([]Chirp, len(keys))
		for i := 0; i < len(keys); i++ {
			chirps[i] = dbs.Chirps[keys[i]]
		}

		return chirps, nil
	}
}

func (db *DB) CreateUser(email string, pwd []byte) (User, error) {
	dbs, err := db.loadDB()
	if err != nil {
		return User{}, err
	}

	var id int
	var ids []int
	for k := range dbs.Users {
		ids = append(ids, dbs.Users[k].ID)
	}

	if len(ids) == 0 {
		id = 1
	} else {
		sort.Slice(ids, func(i, j int) bool { return ids[i] < ids[j] })
		id = ids[len(ids)-1] + 1
	}

	user := User{
		ID:          id,
		Email:       email,
		IsChirpyRed: false,
		Password:    pwd,
	}

	dbs.Users[id] = user

	err = db.writeDB(dbs)
	if err != nil {
		return User{}, err
	}

	return user, nil
}

func (db *DB) GetUser(email string) (User, error) {
	dbs, err := db.loadDB()
	if err != nil {
		return User{}, err
	}

	for _, u := range dbs.Users {
		if u.Email == email {
			return u, nil
		}
	}

	return User{}, nil
}

func (db *DB) UpdateUser(id int, email string, pwd []byte) (User, error) {
	dbs, err := db.loadDB()
	if err != nil {
		return User{}, err
	}

	for i, u := range dbs.Users {
		if u.ID == id {
			dbs.Users[i] = User{
				ID:          id,
				Email:       email,
				IsChirpyRed: u.IsChirpyRed,
				Password:    pwd,
			}

			err = db.writeDB(dbs)
			if err != nil {
				return User{}, err
			}

			return dbs.Users[i], nil
		}
	}

	return User{}, fmt.Errorf("User id not found")
}

func (db *DB) UpgradeUser(id int) error {
	dbs, err := db.loadDB()
	if err != nil {
		return err
	}

	for i, u := range dbs.Users {
		if u.ID == id {
			dbs.Users[i] = User{
				ID:          u.ID,
				Email:       u.Email,
				IsChirpyRed: true,
				Password:    u.Password,
			}

			err = db.writeDB(dbs)
			if err != nil {
				return err
			}

			return nil
		}
	}

	return errors.New("user id not found")
}

func (db *DB) RevokeToken(token string) (Token, error) {
	dbs, err := db.loadDB()
	if err != nil {
		return Token{}, err
	}

	to_revoke := Token{
		ID:         token,
		RevokeTime: time.Now(),
	}

	dbs.Revoked[token] = to_revoke

	err = db.writeDB(dbs)
	if err != nil {
		return Token{}, err
	}

	return to_revoke, nil
}

func (db *DB) CheckRevoked(token string) (Token, error) {
	dbs, err := db.loadDB()
	if err != nil {
		return Token{}, err
	}

	revoked, ok := dbs.Revoked[token]
	if !ok {
		return Token{}, err
	}

	return revoked, nil
}

func (db *DB) ensureDB() error {
	db.mux.Lock()
	defer db.mux.Unlock()

	_, err := os.ReadFile(db.path)
	if errors.Is(err, os.ErrNotExist) {
		dbs := DBStructure{
			Chirps:  make(map[int]Chirp),
			Users:   make(map[int]User),
			Revoked: make(map[string]Token),
		}

		data, err := json.Marshal(dbs)
		if err != nil {
			return err
		}

		err = os.WriteFile(db.path, data, os.ModeExclusive)
		if err != nil {
			return err
		}
	}

	return nil
}

func (db *DB) loadDB() (DBStructure, error) {
	db.mux.Lock()
	defer db.mux.Unlock()

	data, err := os.ReadFile(db.path)
	if err != nil {
		return DBStructure{}, err
	}

	dbs := DBStructure{}
	err = json.Unmarshal(data, &dbs)
	if err != nil {
		return DBStructure{}, err
	}

	return dbs, nil
}

func (db *DB) writeDB(dbStructure DBStructure) error {
	db.mux.Lock()
	defer db.mux.Unlock()

	data, err := json.Marshal(dbStructure)
	if err != nil {
		return err
	}

	err = os.WriteFile(db.path, data, os.ModeExclusive)
	if err != nil {
		return err
	}

	return nil
}
